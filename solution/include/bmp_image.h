#pragma once
#include "util.h"
#include "image.h"

bool load_bmp_image(FILE* file, struct image* out_image);
bool save_bmp_image(FILE* file, const struct image* in_image);
