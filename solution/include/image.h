#pragma once
#include "util.h"

struct __attribute__((packed)) pixel {
	uint8_t r, g, b;
};

struct image {
	struct pixel* pixels;

	uint32_t width;
	uint32_t height;
};

struct image create_image(uint32_t width, uint32_t height);
void destroy_image(struct image* image);


