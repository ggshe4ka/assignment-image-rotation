#include "bmp_image.h"

#include "util.h"

#if defined(_MSC_VER)
#pragma pack(push, 1)
#define ALIGN_1
#else
#define ALIGN_1 __attribute__((packed))
#endif

#define BMP_HEADER_BM 0x4D42 // BMP file starts from a 'BM'

struct ALIGN_1 bmp_header {
    uint16_t bfType;
    uint32_t bfSize;
    uint16_t bfReserved1;
    uint16_t bfReserved2;
    uint32_t bfOffBits;

    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#if defined(_MSC_VER)
#pragma pack(pop)
#endif

enum bmp_pixel_type {
    BPT_RGB        = 0,
    BPT_RLE8       = 1,
    BPT_RLE4       = 2,
    BPT_BITFIELDS  = 3,
    BPT_JPEG       = 4,
    BPT_PNG        = 5,
};

uint8_t padding_size_calculation(uint8_t remainder) {
    return 4 - remainder;
}

bool header_checking(struct bmp_header file_header, size_t file_size) {
    if (file_header.bfType != BMP_HEADER_BM)  return false; // BMP file must start with 'BM'
    if (file_header.bfSize != file_size)      return false;
    if (file_header.biCompression != BPT_RGB) return false;
    return true;
}

bool load_bmp_image(FILE* file, struct image* out_image) {
    assert(file);
    assert(out_image);

    destroy_image(out_image);

    fseek(file, 0, SEEK_END);
    const size_t file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    struct bmp_header file_header;

    if (file_size < sizeof(file_header)) return false;
    fread(&file_header, sizeof(file_header), 1, file);


    if (!header_checking(file_header, file_size)) return false;

    *out_image = create_image(file_header.biWidth, file_header.biHeight);
    if (!out_image->pixels) return false;

    fseek(file, file_header.bfOffBits, SEEK_SET);
    
    const size_t line_size = (size_t)file_header.biWidth * 3;
    const uint8_t remainder = line_size % 4;

    const uint8_t padding_size = padding_size_calculation(remainder);

    for (size_t i = 0; i < (size_t)file_header.biHeight; i += 1) {
        if (!fread((uint8_t*)out_image->pixels + line_size * i, line_size, 1, file)) return false;
        fseek(file, padding_size, SEEK_CUR);
    }

    return true;
}

struct bmp_header bmp_header_initialization(const size_t file_size, const size_t header_size, const struct image* in_image, const size_t image_size) {
    struct bmp_header file_header;
    file_header.bfType      = BMP_HEADER_BM;
    file_header.bfSize      = (uint32_t)file_size;
    file_header.bfReserved1 = 0;
    file_header.bfReserved2 = 0;
    file_header.bfOffBits   = (uint32_t)header_size;

    file_header.biSize          = 40; 
    file_header.biWidth         = in_image->width;
    file_header.biHeight        = in_image->height;
    file_header.biCompression   = BPT_RGB;
    file_header.biSizeImage     = (uint32_t)image_size;
    file_header.biPlanes        = 1;
    file_header.biBitCount      = 24;
    file_header.biXPelsPerMeter = 0;
    file_header.biYPelsPerMeter = 0;
    file_header.biClrUsed       = 0;
    file_header.biClrImportant  = 0;

    return file_header;
}

bool save_bmp_image(FILE* file, const struct image* in_image) {
    const size_t line_size = (size_t)in_image->width * 3;
    const uint8_t remainder = line_size % 4;

    size_t padded_line_size = line_size;
    if (remainder) padded_line_size += (size_t)padding_size_calculation(remainder);

    const size_t image_size  = padded_line_size * in_image->height;
    const size_t header_size = sizeof(struct bmp_header);
    const size_t file_size   = header_size + image_size;

    const struct bmp_header file_header = bmp_header_initialization(file_size, header_size, in_image, image_size);

    if (!fwrite(&file_header, sizeof(file_header), 1, file)) return false;

    uint8_t* pixels = (uint8_t*)in_image->pixels;
    const uint8_t padding_size = padding_size_calculation(remainder);
    
    for (size_t i = 0; i < in_image->height; i += 1) {
        if (!fwrite(pixels + line_size * i, line_size, 1, file)) return false;

        uint64_t padding_to_write = 0;
        if (!fwrite(&padding_to_write, padding_size, 1, file)) return false;
    }

    return true;
}
