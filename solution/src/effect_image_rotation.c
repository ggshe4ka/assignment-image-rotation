#include "util.h"
#include "effect_image_rotation.h"

void rotation(const struct image* in_image, struct image* out_image) {
	// Do checks for in/out image
	assert(in_image);
	assert(out_image);

	destroy_image(out_image);

	*out_image = create_image(in_image->height, in_image->width);
	if (!out_image->pixels) return; // Fail case.

	for (size_t i = 0; i < in_image->width; i += 1) {
		for (size_t j = 0; j < in_image->height; j += 1) {
			// Do stuff of rotating image
			const struct pixel* source_pixel 	= in_image->pixels  + (j * in_image->width + i);
			struct pixel* destination_pixel 	= out_image->pixels + ((i + 1) * in_image->height - 1 - j);

			*destination_pixel = *source_pixel;
		}
	}
}
