#include "image.h"

struct image create_image(uint32_t width, uint32_t height) {

	assert(width  && "Width cannot be zero");
	assert(height && "Height cannot be zero");

	struct image ret = {
		.width = width,
		.height = height,
		.pixels = malloc(width*height*sizeof(struct pixel))
	};

	return ret;
}

void destroy_image(struct image* image) {
	assert(image);

	free(image->pixels);

	image->width  = 0;
	image->height = 0;
	image->pixels = NULL;
}

