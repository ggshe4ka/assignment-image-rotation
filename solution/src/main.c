#include "bmp_image.h"
#include "effect_image_rotation.h"

int main(int argc, char** argv) {
    if (argc < 3) return -1;
    
    FILE* file_src = fopen(argv[1], "rb");
    if (!file_src) {
        fprintf(stderr, "%s\n", "Could not open source file to read");
        return -1;
    }

    FILE* file_dst = fopen(argv[2], "wb");
    if (!file_dst) {
        fprintf(stderr, "%s\n", "Could not open destination file to write");
        return -1;
    }

    struct image image_src = { 0 };
    if (!load_bmp_image(file_src, &image_src)) {
        fprintf(stderr, "%s\n", "Could not load file content as bmp image");
        return -1;
    }

    fclose(file_src);
    
    struct image image_dst = { 0 };
    rotation(&image_src, &image_dst);
    destroy_image(&image_src);

    if (!save_bmp_image(file_dst, &image_dst)) {
        fprintf(stderr, "%s\n", "Could not save bmp image");
        return -1;
    }
    fclose(file_dst);
    destroy_image(&image_dst);

    return 0;
}
